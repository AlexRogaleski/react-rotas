import React from 'react';

const Home = () => {
  const css = {
    backgroundImage: 'url("/assets/image/background.jpeg")',
    height: '100vh'
  };

  return (
    <div className="container-fluid" style={css}>
      <h1>Página Inicial</h1>
    </div>
  );
}

export default Home;