import { React, useState, useEffect } from 'react';
import { useParams } from "react-router-dom";

function User() {
    const { id } = useParams();
    const [user, setUser] = useState({id: '', name: '', age: '', address: ''});

    useEffect(() => {
        const data = [
            { id: 1, name: 'Jack', age: 28, address: 'Rua 1, 1, Centro' },
            { id: 2, name: 'Rose', age: 36, address: 'Rua 2, 15, Centro' },
        ];
        if (id) {
            data.filter(p => p.id === parseInt(id)).map(p => setUser(p));
        }

    }, [id]);

    const handleChange = (event) => {
        const newUser = {...user};
        newUser[event.target.name] = event.target.value;
        setUser(newUser);
    }

    return (
        <div className="container-fluid p-3">
            <div className="card">
                <h5 className="card-header">Usuário</h5>
                <div className="card-body">
                    <form className="row g-3">
                        <div className="col-md-6">
                            <label htmlFor="inputId" className="form-label">Id</label>
                            <input type="text" className="form-control" id="inputId" value={user ? user.id : ""} disabled={true} />
                        </div>
                        <div className="col-md-6">
                            <label htmlFor="inputName" className="form-label">Nome</label>
                            <input type="text" className="form-control" id="inputName" name="name" onChange={handleChange} value={user ? user.name : ""} />
                        </div>
                        <div className="col-md-6">
                            <label htmlFor="inputAge" className="form-label">Idade</label>
                            <input type="text" className="form-control" id="inputAge" name="age" onChange={handleChange} value={user ? user.age : ""} />
                        </div>
                        <div className="col-md-6">
                            <label htmlFor="inputAddress" className="form-label">Endereço</label>
                            <input type="text" className="form-control" id="inputAddress" name="address" onChange={handleChange} value={user ? user.address : ""} />
                        </div>
                        <div className="col-md-12">
                            <button type="submit" className="btn btn-primary">Salvar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    );
}

export default User;