import Table from "../components/Table";

function Users() {
    const columns = [
        {
            title: 'Id',
            key: 'id',
            width: 50,
        },
        {
            title: 'Nome',
            key: 'name',
            width: 100,
        },
        {
            title: 'Idade',
            key: 'age',
            width: 100,
        },
        {
            title: 'Endereço',
            key: 'address',
            width: 200,
        },
        {
            title: '',
            key: 'edit',
            width: 50,
        },
    ];

    const data = [
        { id: 1, name: 'Jack', age: 28, address: 'Rua 1, 1, Centro' },
        { id: 2, name: 'Rose', age: 36, address: 'Rua 2, 15, Centro' },
    ];

    return (
        <div className="container-fuid p-3">
            <div className="card">
            <h5 className="card-header">Usuários</h5>
            <div className="card-body">
                <Table columns={columns} data={data} handlerEdit={true} />
            </div>
        </div>
        </div>
    );
}

export default Users;
