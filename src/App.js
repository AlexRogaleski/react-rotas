import React from 'react';
import { Routes, Route, Navigate } from "react-router-dom";
import MainHeader from './components/MainHeader';

import Home from "./pages/Home";
import Users from "./pages/Users";
import User from "./pages/User";

export default function App() {
  return (
    <>
      <MainHeader />
      <Routes>
        <Route element={<Navigate to="/Home" />} path="/" />
        <Route element={<Home />} path="/home" />
        <Route element={<Users />} path="/users"/>
        <Route element={<User />} path="/users/user/:id"/>
      </Routes>
    </>
  );
}