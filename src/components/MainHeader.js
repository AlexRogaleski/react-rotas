import { NavLink } from "react-router-dom";

function MainHeader() {
    return (
        <header>
            <nav className="navbar navbar-expand-lg navbar-dark bg-dark fixed-to">
                <div className="container-fluid">
                    <a className="navbar-brand" href="/home">React Rotas</a>
                    <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                        <span className="navbar-toggler-icon"></span>
                    </button>
                    <div className="collapse navbar-collapse" id="navbarNavAltMarkup">
                        <ul className="navbar-nav">
                            <li>
                                <NavLink className={(navData) => navData.isActive ? "nav-link active" : "nav-link"} to="/home">Home</NavLink>
                            </li>
                            <li>
                                <NavLink className={(navData) => navData.isActive ? "nav-link active" : "nav-link"} to="/users">Usuários</NavLink>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
        </header>
    );
}
export default MainHeader;