import { Link } from "react-router-dom";

function Table({ columns, data, handlerEdit }) {
    const renderTableHeader = (props) => {
        return props.map((value) => {
            return <th scope='col' width={value.width} key={value.key}>{value.title}</th>
        })
    }

    const renderTableData = (props) => {
        return props.map((line, index) => {
            return (
                <tr key={index}>
                    {Object.values(line).map((value, index) => {
                        return <td key={index}>{value}</td>
                    })}
                    {handlerEdit ? <td key={index}><Link to={`user/${line.id}`}>Editar</Link></td> : null}
                    {/* {handlerEdit ? <td key={index}><a href={`users/user/${line.id}`} className="btn btn-primary btn-sm" >Editar</a></td> : null} */}
                </tr>
            );
        });
    }

    return (
        <table className="table table-striped table-hover">
            <thead>
                <tr>
                    {renderTableHeader(columns)}
                </tr>
            </thead>
            <tbody>
                {renderTableData(data)}
            </tbody>
        </table>
    );
}

export default Table;